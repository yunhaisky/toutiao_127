import axios1 from 'axios'
import store from '@/store'
import Notify from '@/ui/Notify'
import router from '@/router'
import { removeStorage } from '@/storage'
import to from '@/utils/awaitTo' // 把to函数请过来-让它包装住axios请求, 把结果处理成数组再返回到业务页面中
// axios.defaults.baseURL = 'http://toutiao-app.itheima.net'
// axios.defaults.baseURL = 'http://api-toutiao-web.itheima.net/app'
// export default axios // 让外面使用带基地址的axios方法
var JSONbigString = require('json-bigint')({ storeAsString: true })
const axios = axios1.create({
  baseURL: 'http://toutiao-app.itheima.net',
  transformResponse: [function (data) {
    if (data.length !== 0) {
      return JSONbigString.parse(data)
    } else {
      return data
    }
  }]
})

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  if (store.state.token) {
    config.headers.Authorization = 'Bearer ' + store.state.token
  }

  // 在发送请求之前做些什么
  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  return response
}, function (error) {
  // console.dir(error)// 错误对象一般用dir打印
  if (error?.response?.status === 401) {
    Notify({ type: 'danger', message: 'token过期-请重新登录' })
    removeStorage('token')
    store.commit('clearToken')

    setTimeout(() => {
      router.push({
        name: 'Login'
      })
    }, 1000)
  }
  // 对响应错误做点什么
  return Promise.reject(error)
})

// 问题: 如果以后换网络请求库, ajax的$.ajax()
// 配置项: url, type, data
// 解决: 中间加一层函数, 自己处理外面传进来了的值
// 函数形参 - 对象的解构赋值, 一个个值接到变量上
export default ({ url, method = 'GET', data = {}, headers = {}, params = {} }) => {
  const PromiseA = axios({
    url,
    method,
    data,
    headers,
    params
  })
  return to(PromiseA) // 开整-网络请求

  // 假如替换了请求库
  //   return new Promise((resolve, reject) => {
  //     // 如果有params写js原生代码, 把params对象里的值, 拼接到url后面去
  //     $.ajax({
  //       url,
  //       type: method,
  //       data,
  //       headers,
  //       success (res) {
  //         resolve(res)
  //       },
  //       error (err) {
  //         reject(err)
  //       }
  //     })
  //   })
}
