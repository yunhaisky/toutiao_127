// 中间人写法
// function to (PromiseA) {
//   const PromiseB = PromiseA.then(res => {
//     return [null, res]
//   }).catch(err => {
//     return [err, null]
//   })
//   return PromiseB
// }

// export default to

// 极简写法
export default PromiseA => PromiseA.then(res => [null, res]).catch(err => [err, null])
