import router from './index'
import store from '@/store'
router.beforeEach((to, from, next) => {
  if (store.state.token && to.path === '/login') {
    next(false)
  } else {
    next()
  }
})
