import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login'
import Layout from '@/views/Layout'
import Home from '@/views/Home'
import My from '@/views/My'
import Search from '@/views/Search'
import SearchResult from '@/views/Search/SearchResult'
import Article from '@/views/Article'
import UserEdit from '@/views/My/UserEdit'
import Chat from '@/views/Chat'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/layout'
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/layout',
    component: Layout,
    name: 'Layout',
    redirect: '/layout/home',
    children: [
      {
        path: 'home',
        name: 'Home',
        component: Home
      },
      {
        path: 'my',
        name: 'My',
        component: My
      }
    ]
  },
  {
    path: '/search',
    name: 'Search',
    component: Search
  },
  {
    path: '/search_result/:kw',
    name: 'SearchResult',
    component: SearchResult
  },
  {
    path: '/article/:a_id',
    name: 'ArticleDetail',
    component: Article
  }, {
    path: '/my/user',
    name: 'UserEdit',
    component: UserEdit
  }, {
    path: '/chat',
    name: 'Chat',
    component: Chat
  }
]

const router = new VueRouter({
  routes
})

export default router
