import Vue from 'vue'
Vue.directive('focus', {
  inserted (el) {
    if (el.nodeName === 'INPUT' || el.nodeName === 'TEXTAREA') {
      el.focus()
    } else {
      el.querySelector('input').focus()
    }
  }
})
