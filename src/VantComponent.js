import Vue from 'vue'
import { NavBar, Form, Field, Button, Tabbar, TabbarItem, Image as VanImage, Icon, Tabs, Tab, Cell, PullRefresh, List, Lazyload, ActionSheet, Popup, Col, Row, Badge, Search, Divider, Tag, CellGroup, Dialog, DatetimePicker } from 'vant'

Vue.use(DatetimePicker)
Vue.use(Dialog)
Vue.use(CellGroup)
Vue.use(Tag)
Vue.use(Divider)
Vue.use(Search)
Vue.use(Col)
Vue.use(Badge)
Vue.use(Row)
Vue.use(Popup)
Vue.use(ActionSheet)
Vue.use(Lazyload)
Vue.use(List)
Vue.use(PullRefresh)
Vue.use(Cell)
Vue.use(Tab)
Vue.use(Tabs)
Vue.use(Icon)
Vue.use(VanImage)
Vue.use(Tabbar)
Vue.use(TabbarItem)
Vue.use(Field)
Vue.use(Form)
Vue.use(NavBar)
Vue.use(Button)
