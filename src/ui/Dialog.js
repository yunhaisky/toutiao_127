import Vue from 'vue'
import { Dialog } from 'vant'

// 自定义了一个弹出框的方法
const DialogDefault = ({ message, title, showCancelButton }) => {
  return Dialog({
    message,
    title,
    showCancelButton
  })
}

Vue.prototype.$dialog = DialogDefault
