import axios from '@/utils/request'

// 个人 - 基本资料
export const getUserInfo = () => axios({
  url: '/v1_0/user'
})

// 个人 - 个人简介
export const getProfile = () => axios({
  url: '/v1_0/user/profile'
})

// 个人 - 更换头像
export const uploadPhoto = data => axios({
  url: '/v1_0/user/photo',
  method: 'PATCH',
  data
})

// 个人 - 更新名字/生日
export const updateProfile = data => axios({
  url: '/v1_0/user/profile',
  method: 'PATCH',
  data
})
