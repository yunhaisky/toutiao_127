import axios from '@/utils/request'

export const userChannel = () => axios({
  url: '/v1_0/user/channels'
})

// 获取 - 所有频道
export const allChannel = () => axios({
  url: '/v1_0/channels'
})

export const updateChannel = data => axios({
  url: '/v1_0/user/channels',
  method: 'PUT',
  data
})

// 获取 - 首页 - 文章列表
export const getArticleList = params => axios({
  url: '/v1_1/articles',
  params
})

// 文章 - 不感兴趣
export const dislike = data => axios({
  url: '/v1_0/article/dislikes',
  method: 'POST',
  data
})

export const reports = data => axios({
  url: '/v1_0/article/reports',
  method: 'POST',
  data
})
