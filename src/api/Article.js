import axios from '@/utils/request'

// 获取文章详情
export const articleDetail = id => axios({
  url: `/v1_0/articles/${id}`
})

// 文章 - 关注作者
export const followings = data => axios({
  url: '/v1_0/user/followings',
  method: 'POST',
  data
})

// 文章 - 取消关注 - 作者
export const unfollowings = data => axios({
  url: `/v1_0/user/followings/${data.target}`,
  method: 'DELETE'
})

// 文章 - 点赞文章
export const articleAttitude = data => axios({
  url: '/v1_0/article/likings',
  method: 'POST',
  data
})

// 文章 - 取消点赞文章
export const unArticleAttitude = data => axios({
  url: `/v1_0/article/likings/${data.target}`,
  method: 'DELETE'
})

// 评论 - 列表
export const commentList = params => axios({
  url: '/v1_0/comments',
  params
})

// 评论 - 点赞
export const commentLike = data => axios({
  url: '/v1_0/comment/likings',
  method: 'POST',
  data
})

// 评论 - 取消点赞
export const unCommentLike = id => axios({
  url: `/v1_0/comment/likings/${id}`,
  method: 'DELETE'
})

// 评论 - 发布
export const sendComment = data => axios({
  url: '/v1_0/comments',
  method: 'POST',
  data
})
