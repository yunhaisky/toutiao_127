import axios from '@/utils/request'
export const login = data => axios({
  url: '/v1_0/authorizations',
  method: 'POST',
  data
})
