import { login } from './Login'
import { userChannel, getArticleList, dislike, reports, allChannel, updateChannel } from './Home'
import { suggestList, searchREsult } from './Search'
import { articleDetail, followings, unfollowings, articleAttitude, unArticleAttitude, commentList, commentLike, unCommentLike, sendComment } from './Article'
import { getUserInfo, getProfile, uploadPhoto, updateProfile } from './My'

export const loginAPI = login // loginAPI变量里装的是一个函数(内部带着axios请求方法)
export const userChannelAPI = userChannel// 获取 - 用户已选频道
export const getArticleListAPI = getArticleList// 获取 - 文章列表
export const dislikeArticleAPI = dislike// 反馈 - 不感兴趣文章
export const reportsAPI = reports// 反馈 - 举报文章
export const allChannelAPI = allChannel// 获取 - 所有频道
export const updateChannelAPI = updateChannel// 更新 - 用户选择频道
export const suggestListAPI = suggestList// 搜索 - 联想菜单
export const searchREsultAPI = searchREsult// 获取 - 结果列表
export const articleDetailAPI = articleDetail// 获取 - 文章详情
export const followingsAPI = followings// 文章 - 关注 - 作者
export const unfollowingsAPI = unfollowings// 文章 - 取消关注 - 作者
export const articleAttitudeAPI = articleAttitude// 文章 - 点赞文章
export const unArticleAttitudeAPI = unArticleAttitude// 文章 - 取消点赞文章
export const commentListAPI = commentList// 文章 - 评论列表
export const commentLikeAPI = commentLike// 评论 - 点赞
export const unCommentLikeAPI = unCommentLike// 评论 - 取消点赞
export const sendCommentAPI = sendComment// 评论 - 发布
export const getUserInfoAPI = getUserInfo// 个人 - 基本资料
export const getProfileAPI = getProfile// 个人 - 个人简介
export const uploadPhotoAPI = uploadPhoto// 个人 - 更换头像
export const updateProfileAPI = updateProfile// 个人 - 更新名字/生日
